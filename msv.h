#ifndef MSV_H
#define MSV_H

#include<stdlib.h>
#include<string.h>
#include<vector>
#include<algorithm>
#include<bitset>
#include<assert.h>
#include<math.h>
#include <Eigen/Core>
#include <Eigen/Eigen>
#include "utility.h"
using namespace std;
#define WRITE_PER_COMPONENT  1
#define SIMILARITY_EPSILON  1
#define PI M_PI
// Implementation of 'Linear Time Maximally Stable Extremal Regions' David Nister and Henrik Stewenius
// For 3D datasets, datatype unsigned 8bit only  => 255 grey levels
// 6 Neighbour per voxel
#define UNUSED_MAX_GREY 260
#define MDBG(x)// x
class eigenInfo {
public:
    double x;
    double y;
    double z;
    double e;

    friend bool operator<(const eigenInfo &lhs, const eigenInfo  &rhs)
    {
        return lhs.e < rhs.e;
    }
};

int heapHead(std::bitset<256>* bit, bool pass)
{
    if (pass)
    {
        return bit->_Find_first();
    }
    else
    {
        int c = bit->count()-1;
        int start = bit->_Find_first();
        while(c--)
        {
            start = bit->_Find_next(start);
        }
        return start;
    }
}

class MSV; // forward declaration
class MSVParams {
  int xDimension;
  int yDimension;
  int zDimension;
  char filename[80];
  int delta;
  int minVolume;
  int maxVolume;
  int minVariation;
  public:
  MSVParams(char *fname, int x, int y, int z, int d=5, int minV = 100, int maxV=10000, int minVar = 100)
  {
    strcpy(filename,fname);
    xDimension = x;
    yDimension = y;
    zDimension = z;
    delta = d;
    minVolume = minV;
    maxVolume = maxV;
    minVariation = minVar;

  }
  ~MSVParams()
  {

  }
  private:
  char * getFileName()
  {
      return filename;
  }

  void printSelf()
  {
    cout << "xDim: "<<xDimension << endl;
  }

  int getSize()
  {
    return xDimension*yDimension*zDimension;
  }
  int getXDimension()
  {
      return xDimension;
  }
  int getYDimension()
  {
      return yDimension;
  }
  int getZDimension()
  {
      return zDimension;
  }

  friend class MSV;
 // friend class MSVComponent;
} ;


// Each MSV Component that will be pushed on to the stack
// Some of these components will be stable;
class history{
public:
    int grey;
    int size;
    history(int g, int s)
    {
        grey = g;
        size = s;
    }
} ;

class Element{
public:
    int pixel;
    int edge;
    Element(int pix, int e)
    {
        pixel = pix;
        edge = e;
    }
    Element(int pix)
    {
        pixel = pix;
        edge = LEFT;
    }
    Element()
    {
        pixel = 0;
        edge = LEFT;
    }
};

class MSVComponent{
  public:
  int greyValue;
  Element lastElement;
  bool stable;
  double stability;
  int volume;
  vector<int> list;
  vector<int> intensity;
  double moments[19]; //x,y,z,xx,yy,zz,xy,yz,zx, xxx, yyy,zzz, xyz, xyy, xzz,xxy,yzz, xxz,yyz
  double eigenVector1[3];
  double eigenVector2[3];
  double eigenVector3[3];
  double eigen1;
  double eigen2;
  double eigen3;
  double centroid[3];
  bool userflag;
  double huMoments[2];
  double centralMoments[16];
  vector< history> componentHistory;
  MSVComponent *parent;
  MSVComponent *child;
  MSVComponent *sibling;

  bool boundaryHasBeenProcessed;

  MSVComponent(int grey)
  {
    greyValue = grey;
    volume = 0;
    stable = false;
    parent = NULL;
    child  = NULL;
    sibling = NULL;
    stability = UNUSED_MAX_GREY;
    fill_n(moments,19,0.0);
    boundaryHasBeenProcessed = false;
    userflag = false;
  }

  bool  operator==( MSVComponent const &rhs)
  {
      bool isEqual = false;
      bool greyEqual = fabs(fabs(this->greyValue) - fabs(rhs.greyValue)) < 5 ;
      bool volumeEqual = fabs(this->volume - rhs.volume) < 20;
      bool eigenEqual = (fabs(this->eigen1 - rhs.eigen1) < SIMILARITY_EPSILON ) &&
              (fabs(this->eigen2 - rhs.eigen2) < SIMILARITY_EPSILON ) &&
              (fabs(this->eigen3 - rhs.eigen3) < SIMILARITY_EPSILON );
      bool huEqual = fabs(this->huMoments[0] - rhs.huMoments[0]) < SIMILARITY_EPSILON &&
              fabs(this->huMoments[1] - rhs.huMoments[1]) < SIMILARITY_EPSILON;

      // using the volume component contradics with normalizing huMoments
      // but currently huMoments are not as discriminative as they should be
      isEqual = greyEqual && /*volumeEqual && eigenEqual&&*/ huEqual ;
      return isEqual;

  }

  void mergeComponent(MSVComponent *component)
  {
      assert(!component->parent);
      assert(!component->sibling);
      component->parent = this;
      component->sibling = this->child;

      this->child = component;

      this->volume += component->volume;
      for(int i=0; i<19; i++)
      {
          this->moments[i] += component->moments[i];
      }
      // don't bother merging the list, each component has it's own list
      // use volume for computing msv
      // we'll be merging to a higher value always
      assert(component->greyValue <= this->greyValue);
      // This should be the entire volume..

      // For DEBUG Purposes Only:
      // Merging the lists also
      for (int i=0; i < component->list.size(); i++)
      {
          this->list.push_back(component->list[i]);
      }
  }
  
  void accumulate(int pixel, int dX, int dY, int dZ)
  {
      this->volume++;
      list.push_back(pixel);
      int cx,cy,cz;
      LINEAR_TO_GRID(pixel,dX,dY,dZ,cx,cy,cz);
      cx++; cy++; cz++;
      //cout << "cx: " << cx << "cy: " << cy << "cz: " << cz << endl;
      moments[0]+= cx;
      moments[1]+= cy;
      moments[2]+= cz;

      moments[3]+=cx*cx;
      moments[4]+=cy*cy;
      moments[5]+=cz*cz;

      moments[6]+=cx*cy;
      moments[7]+=cy*cz;
      moments[8]+=cz*cx;

      moments[9]  += cx*cx*cx;
      moments[10] += cy*cy*cy;
      moments[11] += cz*cz*cz;

      moments[18] += cx*cy*cz;

      moments[12] += cx*cy*cy;
      moments[13] += cx*cz*cz;
      moments[14] += cx*cx*cy;
      moments[15] += cy*cz*cz;
      moments[16] += cx*cx*cz;
      moments[17] += cy*cy*cz;
  //    cout <<"Accumulating [" << cx << "] "<<"[" << cy << "]" << "[" << cz <<"]"   <<endl;
  }
  void eigen()
  {
      // from the moments compare the eigen-vectors
      // volume = M000
      // moments[] = {M100 M010 M001 M200 M020 M002 M110 M011 M101}
      // we want to compute the eigen-vectors of the matrix
      // [ C200 C110 C101]
      // [ C110 C020 C011]
      // [ C101 C011 C002]


      // Where C stands for Central Moments and M stands for the Raw Moments
      // Central moments can be computed from Raw Moments (M) like so:

      double C100 = this->moments[0]/volume;
      double C010 = this->moments[1]/volume;
      double C001 = this->moments[2]/volume;

      centroid[0] = C100;
      centroid[1] = C010;
      centroid[2] = C001;

      double C200 = this->moments[3] - this->moments[0]*C100;
      double C020 = this->moments[4] - C010*this->moments[1];
      double C002 = this->moments[5] - C001*this->moments[2];

      // c110 = m110 - c100*m010
      double C110 = this->moments[6] - C100*this->moments[1];
      double C011 = this->moments[7] - C010*this->moments[2];
      double C101 = this->moments[8] - C100*this->moments[2];

      //C300 = m300 - 3*C100*M200 -2*C100*C100*M100

      double C300 = this->moments[9]  - 3*C100*this->moments[3] - 2*C100*C100*this->moments[0];
      double C030 = this->moments[10] - 3*C010*this->moments[4] - 2*C010*C010*this->moments[1];
      double C003 = this->moments[11] - 3*C001*this->moments[5] - 2*C001*C001*this->moments[2];

      // C210 = M210 - 2M110*C100 - c010*M200 + 2*C100*C100*M010
      double C210 = this->moments[12] - 2*this->moments[6]*C100 - C010*this->moments[4] + 2*C100*C100*this->moments[1];


      // central moments are translation invariant
      // normalized central moments are scale invariant
      centralMoments[0] = C200/pow(volume,2.0);
      centralMoments[1] = C020/pow(volume,2.0);
      centralMoments[2] = C002/pow(volume,2.0);
      centralMoments[3] = C110/pow(volume,2.0);
      centralMoments[4] = C101/pow(volume,2.0);
      centralMoments[5] = C011/pow(volume,2.0);


      huMoments[0] = centralMoments[0] + centralMoments[1] + centralMoments[2];
      huMoments[1] = pow((centralMoments[0] - centralMoments[1]),2.0) + pow((centralMoments[1] - centralMoments[2]),2.0) + pow((centralMoments[0] - centralMoments[2]),2.0)
              + 4*(pow(centralMoments[3],2.0) + pow(centralMoments[4],2.0), pow(centralMoments[5],2.0));

  //    EIGEN(C200,C020,C002,C110,C011,C101,eigen1,eigen2,eigen3);
  //    eigen1 = sqrt(eigen1);eigen2 = sqrt(eigen2);eigen3 = sqrt(eigen3);

      Eigen::MatrixXd Covariance = Eigen::MatrixXd::Zero(3,3);
      Covariance(0,0) = C200;
      Covariance(1,1) = C020;
      Covariance(2,2) = C002;
      Covariance(0,1) = Covariance(1,0) = C110;
      Covariance(0,2) = Covariance(2,0) = C101;
      Covariance(2,1) = Covariance(1,2) = C011;

      //cout << Covariance << endl;

       // compute the eigenvalue on the Cov Matrix
      Eigen::EigenSolver<Eigen::MatrixXd> m_solve(Covariance);
      Eigen::VectorXd eigenvalues = Eigen::VectorXd::Zero(3);
      eigenvalues = m_solve.eigenvalues().real();

      Eigen::MatrixXd eigenVectors = Eigen::MatrixXd::Zero(3,3);  // matrix (n x m) (points, dims)
      eigenVectors = m_solve.eigenvectors().real();

      eigen1 = eigenvalues(0); eigen2 = eigenvalues(1); eigen2 = eigenvalues(2);
      eigen1 = sqrt(eigen1); eigen2 = sqrt(eigen2); eigen3 = sqrt(eigen3);

    //  cout << "e1: " << eigen1 <<endl<< "e2: " <<eigen2 <<endl<< "e3: " << eigen3 << endl;
  }

  void processBoundary(int dX,int dY,int dZ)
  {
      // process boundaries of stable components
      boundaryHasBeenProcessed = true;

      // for every voxel that is in the component list, we can determine how many nbrs
      // of that voxel is present in the component list. So the nbrhood is purely spatial
      // but the list is based on intensity.

      for(int i = 0; i < this->list.size(); i++)
      {
          int vox = this->list[i];
          int numNbrsInList = 0;
          int cX,cY,cZ;
          LINEAR_TO_GRID(vox,dX,dY,dZ,cX,cY,cZ);
          for(int nbr = LEFT; nbr < ALLNBR; nbr = nbr + 1)
          {
              int nX=cX, nY=cY,nZ=cZ;
              switch(nbr)
              {
               case LEFT:
                  nX = cX > 0 ? cX-1 : cX;
                  break;
                  
              case RIGHT:
                  nX = cX < dX-1 ? 1+cX:cX;
                  break;
                  
              case UP:
                  nY = cY > 0 ? cY -1 :cY;
                  break;
                  
              case DOWN:
                  nY = cY < dY-1 ? 1+cY : cY;
                  break;
              case FRONT:
                  nZ = cZ > 0 ? cZ -1 :cZ;
                  break;
              case BACK:
                  nZ = cZ < dZ-1 ? 1 + cZ : cZ;

                  break;
              case ALLNBR:
                  break;
              }
              
              int newVox = ((nX*dY+nY) * dZ + nZ);
              if(newVox != vox && std::find(this->list.begin(),this->list.end(),newVox) != this->list.end())
                  numNbrsInList++;

            }

            // save this as intenisty at locn vox, so for each voxel we know if it is in the interior
            // since all those voxels are a part of the list or if it is in the boundary.
            // within the boundary we have a way of saying if this voxel is connected to 0 voxels in the interior
            // or one or two or more... how does that help ?
            this->intensity.push_back(numNbrsInList);
      }
  }

  void writeComponentToFile(char *fileName,size_t sz)
  {
      ofstream rawfile;
      unsigned char * compData = new unsigned char[sz];
      fill_n(compData,sz,0);
      for(int i=0; i < this->list.size() ; i++)
      {
          int pixel = this->list[i];
          compData[pixel] = (unsigned char)((std::abs(this->greyValue)) & 0xFF);
      }

      rawfile.open(fileName,ios::binary|ios::out);
      if(rawfile.is_open())
         rawfile.write((char*)compData,sz);
      rawfile.close();
      delete compData;

  }

} ;

class MSV{

  MSVParams *param;
  vector<bool> accessibleMask;
  // priority queue of boundary voxels
  // keep a 256-bit mask for checking which voxels are present
  bitset<256> boundaryBitMask;
  // use the mask to index into the correct list
  vector< vector<Element>* > boundaryList;
  // component stack,
  vector< MSVComponent* > componentStack;
  vector< MSVComponent* > *stableComponents;
  int currentPixel;
  int sourcePixel;
  int currentGreyLevel;
  int currentEdge;

  int minVolume;
  int maxVolume;
  int minVariation;

  // assuming 8-bit grey levels only
  int* data;
  unsigned char *stableData;
  private:
  MSV(){}
  public:
  bool pass;
  MSV(MSVParams * p) {
    param = p;
    boundaryBitMask.reset();
    accessibleMask.resize(param->getSize());// mask for the entire dataset
    currentPixel = sourcePixel = 0;
    data = NULL;
    for(int i = 0 ; i < 256; i++)
    {
        boundaryList.push_back(new vector<Element>() );
    }
    currentEdge = LEFT;
    minVolume = param->minVolume;
    maxVolume = param->maxVolume;
    minVariation = param->minVariation;
    stableData = new unsigned char[param->getSize()];
  }

    void setParams(MSVParams *p){
    param = p;

  }
  
  void readFile()
  {
      char *filename = param->getFileName();
      cout << "MSV:: Attempting to read " << filename << endl;
      char *tempdat = new char[param->getSize()];
      data = new  int[param->getSize()];
      ifstream rawfile;
      rawfile.open(filename,ios::binary|ios::in);
      if(rawfile.is_open())
      {
         rawfile.read(tempdat,param->getSize());
         rawfile.close();
      }
      else
      {
          cout << "No file by the name: " << filename << " found" << endl;
          delete[] tempdat;
      }
        //memcpy(data,tempdat,param->getSize());
      for(int i = 0 ; i < param->getSize(); i++)
      {
          unsigned char c = (unsigned char) tempdat[i];
          data[i] = (int)c;
      }
      delete[] tempdat;


  }

  void processStack(int newGreyValue)
  {

      process_step_1:
      //cout << "Processing Stack for new-grey-value: " << newGreyValue << endl;
      // 1. Process the component of the top of the stack.
      // We didn't do this yet.. processing the component on top of the stack implies
      // trying to identify it as an mser ??



      // nextGrey = min(newGrey, second-top)
      int secondTopGreyValue = componentStack[componentStack.size()-2]->greyValue;
      //int nextGreyValue = std::min(newGreyValue,secondTopGreyValue);
      // 2. if new < second, set top grey to new grey and return
      // This occurs when the new pixel is at a grey value for which component has not
      // been instantiated, set top to new and return WHAT????
      if( newGreyValue < secondTopGreyValue)
      {
          //DEBUG
          MDBG(
          {
              cout <<"       New Grey level (of the boundary) is less than secondtop =  "<< secondTopGreyValue<< endl;
              cout<<"        Adding a new component and immediately merging with top so that heirarchy is maintained" << endl;

          });

          componentStack[componentStack.size()-1]->componentHistory.push_back(history(componentStack[componentStack.size()-1]->greyValue,
                                                                   componentStack[componentStack.size()-1]->volume));
          MSVComponent *newTop = new MSVComponent(newGreyValue);
          MSVComponent *oldTop = componentStack[componentStack.size()-1];
          newTop->mergeComponent(oldTop);
          componentStack.pop_back();
          componentStack.push_back(newTop);

          return;
      }
      //3. Merge top + second
      {
        MSVComponent* top = componentStack[componentStack.size()-1];
          MSVComponent* secondtop = componentStack[componentStack.size()-2];
          //DEBUG
          MDBG(
          {
              cout <<"       New Grey level (of the boundary) is not less than secondtop "<< endl;
              cout<<"        Merging top @ "<< top->greyValue <<"and second top @" << secondtop->greyValue << endl;

          });
          componentStack.pop_back();
          componentStack.pop_back();
          //cout << "Merging components with grey: " << top->greyValue << " and " << secondtop->greyValue << endl;
          secondtop->componentHistory.push_back(history(top->greyValue,top->volume));
          secondtop->componentHistory.push_back(history(secondtop->greyValue,secondtop->volume));
          // merge previous history
          if(top->componentHistory.size() > 0)
          {
              int sz = top->componentHistory.size();
              while(!top->componentHistory.empty())
              {
                  secondtop->componentHistory.push_back(top->componentHistory[sz-1]);
                  top->componentHistory.pop_back();
                  sz--;
              }
          }
          secondtop->mergeComponent(top);
          componentStack.push_back(secondtop);

      }
      //4. if new grey > top grey goto 1
      int topGrey = componentStack[componentStack.size()-1]->greyValue;
      if (newGreyValue  > topGrey)
          goto process_step_1;

  }
  MSVParams* getParams(){
    return param;
  }
  void processForRegions(MSVComponent *component, int delta)
  {

      if(!component)
      {
          return;
      }
      MSVComponent *currentComponent = component;
      MSVComponent *higher = currentComponent->parent;
      MSVComponent *lower = currentComponent->child; //has to be some child, can be child->sibling also
      MSVComponent *lowerStart = lower;
      int upperDelta = currentComponent->greyValue + delta;
      int lowerDelta = currentComponent->greyValue - delta;
      while(higher && higher->greyValue < upperDelta)
      {
          higher = higher->parent;
      }
      bool foundLowerChild = false;
      while( lowerStart && !foundLowerChild)
      {
          lower = lowerStart;
          while(lower && lower->greyValue > lowerDelta)
          {
              lower = lower->child;

          }
          if(lower && lower->greyValue <= lowerDelta)
              foundLowerChild = true;
          if(!foundLowerChild && !lower)
              lowerStart = lowerStart->sibling;
      }
      int impossibeVolume = param->getSize()+1;
      int highVal = impossibeVolume;
      if (higher && higher->greyValue >= upperDelta)
      {
          highVal = higher->volume;
      }
      int lowVal = impossibeVolume;
      if(lower && lower->greyValue <= lowerDelta)
      {
          lowVal = lower->volume;
      }
      if(highVal != impossibeVolume && lowVal != impossibeVolume)
      {

           currentComponent->stability = std::abs(highVal - lowVal)/currentComponent->volume;
      }
      else
      {
          currentComponent->stability = UNUSED_MAX_GREY; // This will never be stable
      }


      MDBG(cout << endl<<"    " << currentComponent->greyValue << "   volume =  "<<currentComponent->volume <<endl);
      // find components (highest) greyValue-delta and (lowest) greyValue+delta


      MSVComponent *child = component->child;
      while(child)
      {
        processForRegions(child, delta);
        child = child->sibling;
      }

  }

  void detectRegions(MSVComponent *component)
  {
      MSVComponent *currentComponent = component;
      if(!currentComponent )
          return;
      MSVComponent *higher = currentComponent->parent;
      MSVComponent *lower = currentComponent->child; //have to find some child could be  child->sibling
      bool variationOk = true;
      bool volumeOk =  (currentComponent->volume > minVolume && currentComponent->volume < maxVolume ) ? true:false;
      MSVComponent *parent = currentComponent->parent;
      while(parent )
      {
          if(!parent->stable)
          {
              parent = parent->parent;
              continue;
          }
          if(std::abs(parent->volume - currentComponent->volume)  < minVariation)
              variationOk = false;
          break;
      }
      //variationOk = volumeOk = true;
      //currentComponent->stable = variationOk && volumeOk;
      if(variationOk && volumeOk && higher && currentComponent->stability <= higher->stability )
      {
          currentComponent->stable = true;
          //cout << "Component with greyValue = " << currentComponent->greyValue <<" and volume= "<< currentComponent->volume<<" is a stable component" << endl;
      }
      bool canBeStable = false;
      if(currentComponent->stable && lower)
      {
          if(currentComponent->stability <= lower->stability)
          {
              canBeStable = true;

          }
          if(!canBeStable)
          lower = lower->sibling;
      }

     currentComponent->stable = currentComponent->stable && canBeStable && (currentComponent->stability < UNUSED_MAX_GREY);
      if(currentComponent->stable)
          cout << "Component with greyValue = " << currentComponent->greyValue <<" and volume= "<< currentComponent->volume<<" is a stable component" << endl;

      MSVComponent *child = component->child;
      while(child)
      {
        detectRegions(child);
        child = child->sibling;
      }
  }

  void writeRegions(MSVComponent *component)
  {
      static int writeId = 0;
      if(!component)
          return;
      if(component->stable)
      {
          {
     
#ifdef WRITE_PER_COMPONENT
              fill_n(stableData,param->getSize(),0);
#endif
              for(int i=0; i < component->list.size() ; i++)
              {
                  int pixel = component->list[i];
                  //unsigned char pixel_intensitiy = (unsigned char)(component->intensity[i]*component->intensity[i]*2);
                  stableData[pixel] = (unsigned char)((std::abs(component->greyValue)) & 0xFF); // want to always set the upper most grey level

              }
              // stores only stable components, ensure they have their eigen values computed
              component->eigen();
              stableComponents->push_back(component);

              //write the stable data into a file
 #ifdef WRITE_PER_COMPONENT
              ofstream rawfile;
              char str[120];
              strcpy( str, param->filename);
              int len = strlen(str);
              str[len-4] = NULL;
              ostringstream suffix;
              suffix <<"_id_" <<  ++writeId<< "_grey_" <<std::abs(component->greyValue) ;

              strcat(str,suffix.str().data());

              if(pass)
                strcat(str,".stable_forward.raw");
              else
                  strcat(str,".stable_backward.raw");
              rawfile.open(str,ios::binary|ios::out);
              if(rawfile.is_open())
                 rawfile.write((char*)stableData,param->getSize());
              rawfile.close();

#endif
          }

      }

      MSVComponent *child = component->child;
      while(child)
      {
        writeRegions(child);
        child = child->sibling;
      }
  }

  void saveOutputFile(bool pass)
  {
      ofstream rawfile;
      char str[80];
      strcpy( str, param->filename);
      int len = strlen(str);
      str[len-4] = NULL;
      if(pass)
        strcat(str,".stable_forward.raw");
      else
          strcat(str,".stable_backward.raw");
      rawfile.open(str,ios::binary|ios::out);
      if(rawfile.is_open())
         rawfile.write((char*)stableData,param->getSize());
      rawfile.close();

  }



  bool execute(bool pass, vector<MSVComponent *> *stable){

    cout << "Should execute msv" << endl;
    this->pass = pass;
    this->stableComponents = stable;
    // make sure we have the data read
    if( !data )
    {
        readFile();
    }
    if (!pass)
    {
        for (int i = 0; i < param->getSize(); i++)
        {
            data[i] = -1*data[i];
        }
    }

    // what needs to be executed here ?

    // Perform Steps 1- 5
    // 1. Clear the accessible pixel mask, the heap of boundary pixels and the component stack. (done)
    //    Push a dummy component onto the stack with grey value higher than any allowed in the image (say 260)
    componentStack.push_back(new MSVComponent(UNUSED_MAX_GREY));

    // 2. Make the source pixel with its first edge the current pixel, mark it as acessible and store the grey level
    // of it in the variable current grey level
    int cX, cY , cZ;
    cX = cY = cZ = 0;
    int dX = param->getXDimension();
    int dY = param->getYDimension();
    int dZ = param->getZDimension();

    currentPixel = sourcePixel = 0;

    //DEBUG
    MDBG(
    {
        cout << "Exploring: " << currentPixel << " @ " << (int)data[currentPixel] << endl;
    });

    accessibleMask[currentPixel] = true;

    // 3. Push an empty component with current level onto the stack
main_step_3:
    currentGreyLevel = data[currentPixel];
    componentStack.push_back(new MSVComponent(currentGreyLevel));
    //DEBUG
    MDBG(
    {
        cout << "Exploring: " << currentPixel << " @ " << (int)data[currentPixel] << endl;
        cout << "New Component added @" << currentGreyLevel << endl;
    });
main_step_4:
    // 4. Explore remaining edges ( neighbours ) of the current pixel in order :
    // for each neighbour, check if accessible, if not, mark accessible and retrieve intensity - new grey level
    // new grey level > current grey level ? add new to boundary list : add current to boundary, set current to new pixel go to 3
    //int currentEdge = LEFT;
    for(int nbr = currentEdge; nbr < ALLNBR; nbr = nbr + 1)
    {
        LINEAR_TO_GRID(currentPixel,dX,dY,dZ,cX,cY,cZ);
        int nX=cX, nY=cY,nZ=cZ;
        int newPixel = currentPixel;
        currentEdge = nbr;
        switch(nbr)
        {

        case LEFT:
            nX = cX > 0 ? cX-1 : cX;

            break;
        case RIGHT:
            nX = cX < dX-1 ? 1+cX:cX;
            break;
        case UP:
            // up
            nY = cY > 0 ? cY -1 :cY;
            break;
        case DOWN:

            // down
            nY = cY < dY-1 ? 1+cY : cY;
            break;
        case FRONT:
            // front
            nZ = cZ > 0 ? cZ -1 :cZ;
            break;
        case BACK:

            // back
            nZ = cZ < dZ-1 ? 1 + cZ : cZ;

            break;
        case ALLNBR:
            break;
        }
        newPixel = ((nX*dY+nY) * dZ + nZ);


        //DEBUG
        MDBG(
        {
            cout << "Next Pixel: " << newPixel << " @ " << (int)data[newPixel] << endl;

        });
        if (newPixel == currentPixel )
        {
            //DEBUG
        MDBG(
            {
                cout <<" Next Pixel Identical to Previous, Looking at next neigbour" << endl;
            });
            continue;
        }
        if(!accessibleMask[newPixel])
        {

            //DEBUG
        MDBG(
            {
                cout <<" Next Pixel has not been accessed, exploring further.." << endl;
            });
            accessibleMask[newPixel] = true;
           // cout << "acc mask @ " << newPixel <<"is : "<< accessibleMask[newPixel] << endl;

            int newGreyLevel = data[newPixel];

            if ( newGreyLevel >= currentGreyLevel)
            {
                // We haven't begun exploring this pixel but we will do so later, beginning from
                // its first edge.
                int idx = pass ? newGreyLevel : -1* newGreyLevel;
                boundaryList[idx]->push_back(Element(newPixel,LEFT));
                boundaryBitMask.set(idx);
                //DEBUG
                MDBG(
                {
                    cout <<"   Adding Next Pixel to boundary since grey value= "<<newGreyLevel <<" is not lower than current="<<currentGreyLevel << endl;
                });

            }
            else
            {
                // Found a better pixel to start exploring, put current pixel on heap with the next edge
                // to explore later and now start exploring the new pixel.

                //DEBUG
            MDBG(
                {
                    cout <<"   Next Pixel better than current as= "<<newGreyLevel <<" is  lower than current="<<currentGreyLevel << endl;
                    cout <<"   Adding current pixel to boundary list and setting new pixel as current " << endl;
                });
                int idx = pass ? currentGreyLevel : -1* currentGreyLevel;
                boundaryList[idx]->push_back(Element(currentPixel,(currentEdge+1)%ALLNBR));
                boundaryBitMask.set(idx);
                currentPixel = newPixel;
                currentEdge = LEFT; // begin exploring this new pixel from its first neighbour
                currentGreyLevel = newGreyLevel;
                goto main_step_3;
            }
        }

    }
    // 5. Accumulate the current pixel to the component on the top of the stack.
    MSVComponent *component = componentStack[componentStack.size()-1];
    component->lastElement = Element(currentPixel, (currentEdge+1));
    component->accumulate(currentPixel,dX,dY,dZ);
    //DEBUG
    MDBG(
    {
        cout <<"     Finished exploring all neighbours of current pixel " << currentPixel << "@" <<currentGreyLevel<<endl;
        cout <<"     Accumulating it and now looking at boundary heap " << endl;
    });

    // 6. Pop boundary pixels, if heap is empty done!
    if(boundaryBitMask.none())
    {
        MSVComponent *entireVolume = componentStack[componentStack.size()-1];
        cout<< "Final Volume: "<< entireVolume->volume << endl;
        // assert that the final stack component indeed has the entire volume
        assert(entireVolume->volume == param->getSize());
        // at this point process this final component for MSERs and Component Trees
        // Computing the MSER can also be done 'while' exploring, but currently do it now

        processForRegions(entireVolume, param->delta);
        detectRegions(entireVolume);
        writeRegions(entireVolume);
        saveOutputFile(pass);

        return true;
    }
    int listIdx = heapHead(&boundaryBitMask,pass);/*pass ? boundaryBitMask._Find_first() : boundaryBitMask.count();*/
    int boundaryListSz = boundaryList[listIdx]->size();

    Element boundaryElement = boundaryList[listIdx]->at(boundaryListSz-1);
    int boundaryPixel = boundaryElement.pixel;
    boundaryList[listIdx]->pop_back();
    if (boundaryList[listIdx]->size()==0)
        boundaryBitMask.set(listIdx,false);
    // if boundary_grey == current grey goto step 4
    //DEBUG
    MDBG(
    {
        cout<<"    Boundary Pixel = " << boundaryPixel << "@" << (int)data[boundaryPixel] << endl;
    });
    if(data[boundaryPixel] == currentGreyLevel)
    {
        currentPixel = boundaryPixel;
        currentEdge = boundaryElement.edge;
        //DEBUG
        MDBG(
        {
            cout <<"       Boundary Pixel has same grey level as current so going ahead and exploring further"<<endl;
            cout <<"       _NO_ component created, already exists on top of stack" << endl;
            cout <<"       Exploring this pixel from its last to be visited edge = " << currentEdge << endl;
        });
        goto main_step_4;
    }
    else if(data[boundaryPixel] > currentGreyLevel) // no way it is less
    {
        // if boundary_grey  > current grey :
        //      process all components on stack until we reach higher grey level
        //      then go to step 4
        //DEBUG
        MDBG(
        {
            cout <<"       Boundary Pixel has grey value = "<< (int)data[boundaryPixel] << "greater than current= "<<currentGreyLevel<<endl;
            cout <<"       Processing stack components and then " << endl;
            cout <<"       Exploring this pixel from its last to be visited edge = " << boundaryElement.edge << endl;
        });
        processStack(data[boundaryPixel]);
        currentPixel = boundaryPixel;
        currentEdge = boundaryElement.edge;
        currentGreyLevel = data[boundaryPixel];
        goto main_step_4;
    }

    return false;
  }

};
#endif // MSV_H
