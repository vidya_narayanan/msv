#include "msv.h"
#include "sstream"
#define EPS 0.001
#define FIND_SIMILAR 1


double dist(double a[3], double b[3])
{
    double d = (a[0] - b[0])*(a[0] - b[0])+(a[1] - b[1])*(a[1] - b[1]) + (a[2] - b[2])*(a[2] - b[2]);
    return sqrt(d);
}


int main(int argc, char* argv[])
{
  
  if (argc < 9)
  {
    cout <<"Usage : " <<argv[0] <<" inputfile.raw xDim yDim zDim delta minVolume maxVolume minVariation" << endl;
    return 0;
  }

  int xDimension = atoi(argv[2]);
  int yDimension = atoi(argv[3]);
  int zDimension = atoi(argv[4]);
  int delta      = atoi(argv[5]);
  int minVolume  = atoi(argv[6]);
  int maxVolume  = atoi(argv[7]);
  int minVariation= atoi(argv[8]);

  MSVParams *mParam = new MSVParams(argv[1], xDimension, yDimension, zDimension,delta, minVolume, maxVolume, minVariation);
  MSVParams *mParamML = new MSVParams("/home/vidya/Datasets/brain_noisy.raw", xDimension, yDimension, zDimension,delta, minVolume, maxVolume, minVariation);

  MSV forwardMSV(mParam);
  MSV backwardMSV(mParam);

  vector <MSVComponent *> stableComponents;

  forwardMSV.execute(true, &stableComponents); //forward pass
  backwardMSV.execute(false, &stableComponents); // backward pass

  cout << "stable components =  " << stableComponents.size() <<endl;
// Estimate similar components
  #ifdef FIND_SIMILAR
  {
      int static cnt = 0;
      for(int i=0; i < stableComponents.size();i++)
      {
          bool foundSomething = false;
          stringstream str;
          MSVComponent a = *(stableComponents[i]);
          if(a.userflag)
              continue;
          for(int j=i+1; j < stableComponents.size(); j++)
          {
              MSVComponent b = *stableComponents[j];
              // we don't want different layers of the same region so check that their
              // centroids are not too nearby
              if( !b.userflag && i!=j &&  a==b && dist(a.centroid,b.centroid) > (xDimension+yDimension+zDimension)*0.01/3)
              {
                  foundSomething = true;
                  cout << "found similar components: " <<i << "and : " << j <<endl;
                  //similarityList[i].push_back(j);
                  str.str(std::string());
                  str << "similar_"<<i<<"_"<<++cnt<<".raw";
                  b.writeComponentToFile((char*)str.str().data(),xDimension*yDimension*zDimension);
                  b.userflag = true;
              }

          }

          if(foundSomething)
          {
              str.str(std::string());
              str << "similar_"<<i<<"_"<<++cnt<<".raw";
              a.writeComponentToFile((char*)str.str().data(),xDimension*yDimension*zDimension);
              a.userflag = true;
          }
      }
  }
#endif
  delete mParam;
  delete mParamML;
}
